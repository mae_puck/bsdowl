### bps.credentials.mk -- Credential switch

# Author: Michael Grünewald
# Date: Sat 29 Mar 2008 16:05:16 CET

# BSD Owl Scripts (https://bitbucket.org/michipili/bsdowl)
# This file is part of BSD Owl Scripts
#
# Copyright © 2005–2014 Michael Grünewald
#
# This file must be used under the terms of the CeCILL-B.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt


### SYNOPSIS

# USE_SWITCH_CREDENTIALS = yes
#
# .include "bps.credentials.mk"


### DESCRIPTION

# Implement a credential switch which applies to the targets
# enumerated in the _SWITCH_CREDENTIALS_TARGETS variable, e.g. install.

#
# Variable description
#

# USE_SWITCH_CREDENTIALS
#
#  Command the use of the switch credential functionality
#
#  Possible values are 'yes' or 'no', it defaults to 'yes'.

# _SWITCH_CREDENTIALS_TARGETS
#
#  List of targets needing a credential switch
#
#  It defaults to the empty list, except for the user is inable to
#  write in ${DESTDIR}${PREFIX}, in which case the install target is
#  added to this list.


### IMPLEMENTATION

.if !target(__<bps.credentials.mk>__)
__<bps.credentials.mk>__:

#
# Variables
#

USE_SWITCH_CREDENTIALS?= yes

_SWITCH_CREDENTIALS_TARGETS?=

# Add the `install` target if the current user is not allowed to write
# under ${DESTDIR}${PREFIX}.

_SWITCH_CREDENTIALS.install!= if [ ! -w /${DESTDIR}${PREFIX} ]; then echo install; else echo ''; fi

.if !empty(_SWITCH_CREDENTIALS.install)
_SWITCH_CREDENTIALS_TARGETS+= ${_SWITCH_CREDENTIALS.install}
.endif


#
# Credential switch
#

.if(${USE_SWITCH_CREDENTIALS} == yes)&&!(${UID} == 0)
.for target in ${_SWITCH_CREDENTIALS_TARGETS}
.if !target(${target})
${target}:: ${target}-switch-credentials
	${NOP}
${target}-switch-credentials:
	${INFO} 'Switching to root credentials for target (${target})'
	@${SU} root -c '${MAKE} ${target}'
.endif
.endfor
.endif

.endif # !target(__<bps.credentials.mk>__)

### End of file `bps.credentials.mk'
