### tex.doc.pre.mk -- Produce TeX documents

# Author: Michael Grünewald
# Date: sam 16 avr 2011 22:44:59 CEST
# Cookie: SYNOPSIS TARGET VARIABLE EN DOCUMENTATION

# BSD Owl Scripts (https://bitbucket.org/michipili/bsdowl)
# This file is part of BSD Owl Scripts
#
# Copyright © 2005–2014 Michael Grünewald
#
# This file must be used under the terms of the CeCILL-B.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt

.include "tex.init.mk"
.include "bps.init.mk"
.include "tex.mpost.mk"

### End of file `tex.doc.pre.mk'
